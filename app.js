const express = require('express')
const app = express()

const Bot = require('keybase-bot')
const bot = new Bot()

var readConfig = require('read-config'),
config = readConfig('config.json');

console.log('user: ' + config.username);
console.log('channel: ' + config.channel.name + ' ' + config.channel.topicName);

bot.init(config.username, config.paperkey, {verbose: false}).then(() => {
	console.log(`Bot is initialized. It is logged in as ${bot.myInfo().username}`)
})
.catch(error => {
	console.error(error)
	bot.deinit()
});

app.get('/', function(req, res){
	if ( req.query.message) {
		bot.chat.send(config.channel, { body: req.query.message }).then(() => {
			console.log('Message "' + req.query.message + '" sent!')
		}).catch(error => {
			console.error(error)
		})
		res.send('published message: ' + req.query.message + ' to keybase');
	}
	else { 
		res.send('missing message');
	}
});

app.listen(config.port, () => console.log(`keybase-alerts app listening on port ${config.port}!`))
